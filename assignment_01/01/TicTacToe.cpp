#include <iostream>
#include <cstdlib>
#include <ctime>
#include "SquareLabel.cpp"
using namespace std;

class TicTacToe
{
  private:
    int board[3][3];
    int player;
    int autoplay;
    SquareLabel label;

  public:
    TicTacToe()
    {
        newGame();
    }

    void newGame()
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                board[i][j] = 0;
            }
        }

        cout << "Enter \n0 for Player vs Player\n1 for Player vs Computer\n: ";
        cin >> autoplay;
        srand(time(NULL));
    }

    void startGame()
    {
        player = 1;
        int nextMove;
        int count = 0;

        do
        {
            showBoard();
            if (!autoplay)
            {
                nextMove = getMove();
            }
            else
            {
                if (player == 1)
                {
                    nextMove = getMove();
                }
                else
                {
                    nextMove = compMove();
                }
            }

            if (isValidMove(nextMove))
            {
                makeMove(nextMove);
                count++;
            }
            else
            {
                cout << endl
                     << "------  P L E A S E  E N T E R  A  V A L I D  M O V E  -----" << endl;
                nextPlayer();
            }

            if ((count >= 9))
            {
                showBoard();
                cout << "Draw !!" << endl;
                return;
            }

        } while (!isPlayerWon());
        showBoard();
        cout << "Player " << player << " Won!!" << endl;
        ;
    }

    int getMove()
    {
        int number;
        cout << "player " << player << " : ";
        cin >> number;
        return number;
    }

    int compMove()
    {
        int random;
        while (1)
        {
            random = (rand() % 9) + 1;
            if (isValidMove(random))
                return random;
        }
    }

    bool isValidMove(int nextMove)
    {
        if (nextMove >= 1 && nextMove <= 9)
        {
            if (!(board[label.row(nextMove)][label.col(nextMove)]))
            {
                return true;
            }
        }
        return false;
    }

    void makeMove(int move)
    {
        board[label.row(move)][label.col(move)] = player;
    }

    bool isPlayerWon()
    {
        //row
        if (boardStatus(1) == player && boardStatus(2) == player && boardStatus(3) == player)
            return true;

        if (boardStatus(4) == player && boardStatus(5) == player && boardStatus(6) == player)
            return true;

        if (boardStatus(7) == player && boardStatus(8) == player && boardStatus(9) == player)
            return true;

        //column
        if (boardStatus(1) == player && boardStatus(4) == player && boardStatus(7) == player)
            return true;

        if (boardStatus(2) == player && boardStatus(5) == player && boardStatus(8) == player)
            return true;

        if (boardStatus(3) == player && boardStatus(6) == player && boardStatus(9) == player)
            return true;

        //diagonal
        if (boardStatus(3) == player && boardStatus(5) == player && boardStatus(7) == player)
            return true;

        if (boardStatus(1) == player && boardStatus(5) == player && boardStatus(9) == player)
            return true;

        nextPlayer();
    }

    int boardStatus(int n)
    {
        return board[label.row(n)][label.col(n)];
    }

    void nextPlayer()
    {
        if (player == 1)
        {
            player = 2;
        }
        else
        {
            player = 1;
        }
    }

    void showBoard()
    {

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                cout << board[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl
             << endl;
    }

    void showGuide()
    {
        cout << "----  B o x    L a b e l   G u i d e  ----" << endl;
        for (int i = 0; i < 9; i++)
        {
            if (i % 3 == 0)
                cout << endl
                     << "\t\t";
            cout << i + 1 << " ";
        }
        cout << "\n\n------------------------------------------";
        cout << "\n----- Enter any number between 1 to 9 ----";
        cout << "\n------     To   S T A R T  playing   -----" << endl
             << endl;
    }
};