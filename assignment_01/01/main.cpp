#include <iostream>
#include "TicTacToe.cpp"

int main()
{
    TicTacToe game;
    game.showGuide();
    game.startGame();

    return 0;
}