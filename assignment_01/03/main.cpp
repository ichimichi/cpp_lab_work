#include <iostream>
#include <limits>
using namespace std;
#include "Account.cpp"

int main(){
    // to avoid displaying in scientific form if
    // balance is large (e.g, 100000000 )
    cout.precision( numeric_limits<double>::digits10 + 1);
    cout<<"Savings Account :"<<endl;
    SavingsAccount myAccount;
    myAccount.setIntRate(7.5);
    myAccount.setBalance(1000);
    cout<<myAccount.getBalance()<<"\tInitial Deposit"<<endl;
    myAccount.credit(myAccount.calculateInterest());
    cout<<myAccount.getBalance()<<"\tInterest Credited"<<endl;
    myAccount.debit(500);
    cout<<myAccount.getBalance()<<"\tamount Debited"<<endl;
    myAccount.credit(50);
    cout<<myAccount.getBalance()<<"\tamount Credited"<<endl;

    cout<<endl<<endl;

    cout<<"Checking Account:"<<endl;
    CheckingAccount otherAccount;
    otherAccount.setFeePerTr(5.8);
    otherAccount.setBalance(1000);
    cout<<otherAccount.getBalance()<<"\tInitial Deposit"<<endl;
    otherAccount.debit(500);
    cout<<otherAccount.getBalance()<<"\tamount Debited"<<endl;
    otherAccount.credit(50);
    cout<<otherAccount.getBalance()<<"\tamount Credited"<<endl;



    return 0;
}