#include <iostream>
using namespace std;

// Base class 
class Account
{
  private:
    double balance;

  public:
    // contructor which calls setBalance() function
    // sets balance to 0 by default
    // unless specified
    Account(double initialBalance = 0.0)
    {
        setBalance(initialBalance);
    }

    void setBalance(double initialBalance)
    {
        balance = initialBalance;
    }

    // adds amount to balance
    void credit(double amount)
    {
        balance += amount;
    }

    // subtracts amount from balance
    bool debit(double amount)
    {
        if (amount <= balance)
        {
            balance -= amount;
            return true;
        }
        else
        {
            cout << "debit amount exceeded account balance." << endl;
            return false;
        }
    }

    // returns the account balance
    double getBalance()
    {
        return balance;
    }

};

// Derived class
class SavingsAccount : public Account
{
  private:
    double interestRate;

  public:
    // calculates the interest amount
    double calculateInterest()
    {
        return (interestRate * getBalance())/100;
    }

    // sets the interest rate
    void setIntRate(double rate){
        interestRate = rate ;
    }
};

// Derived class
class CheckingAccount : public Account
{
    private:
    double feePerTranscn;

    public:
    // redefine credit() to charge transaction fee
    // per transanction
    void credit(double amount){
        Account::credit(amount);
        Account::debit(feePerTranscn);
    }

    
    void debit(double amount){
        // when transaction is performed successfully
        // transaction fee will be charged
        if(Account::debit(amount)){
            Account::debit(feePerTranscn);
        }
    }

    // sets the transaction fee of the account
    void setFeePerTr(double fee){
        feePerTranscn = fee;
    }

};