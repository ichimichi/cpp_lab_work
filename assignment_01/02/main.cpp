// Driver Program

#include <iostream>
using namespace std;
#include "Package.cpp"

int main(){
    TwoDayPackage package1(49.9);
    package1.setSender("Laribok","Mawrie Complex, Rynjah","Shillong","Meghalaya","793006");
    package1.setRecipient("Ichimichi","Greater Noida Expressway, Sector 135","Noida","Uttar Pradesh","201301");
    package1.setWeight(15.2);
    package1.setCostPerKg(499.9);

    cout<<"Total shipping Cost for Two-day package 1: ";
    cout<<package1.calculateCost()<<endl;

    OvernightPackage package2(49.9);
    package2.setSender("Laribok","Mawrie Complex, Rynjah","Shillong","Meghalaya","793006");
    package2.setRecipient("Ichimichi","Greater Noida Expressway, Sector 135","Noida","Uttar Pradesh","201301");
    package2.setWeight(15.2);
    package2.setCostPerKg(499.9);

    cout<<"Total shipping Cost for over-night package 2: ";
    cout<<package2.calculateCost()<<endl;


    return 0;
}