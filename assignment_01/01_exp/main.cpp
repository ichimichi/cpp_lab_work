// Driver Program

#include <iostream>
#include "TicTacToe.h"

int main()
{
    TicTacToe game; // game object of TicTacToe class
    game.showGuide(); // show the guide for the first time
    game.startGame(); // starts the game 

    return 0;
}