// source code of our TicTacToe class
#include <iostream>
#include <cstdlib>  // for rand() and srand()
#include <ctime>    // for time()
#include "TicTacToe.h"
using namespace std;

SquareLabel::SquareLabel()
{
    squareNo[0].i = 0;
    squareNo[0].j = 0;
    squareNo[1].i = 0;
    squareNo[1].j = 1;
    squareNo[2].i = 0;
    squareNo[2].j = 2;
    squareNo[3].i = 1;
    squareNo[3].j = 0;
    squareNo[4].i = 1;
    squareNo[4].j = 1;
    squareNo[5].i = 1;
    squareNo[5].j = 2;
    squareNo[6].i = 2;
    squareNo[6].j = 0;
    squareNo[7].i = 2;
    squareNo[7].j = 1;
    squareNo[8].i = 2;
    squareNo[8].j = 2;
}

int SquareLabel::row(int n)
{
    return squareNo[n - 1].i;
}

int SquareLabel::col(int n)
{
    return squareNo[n - 1].j;
}

TicTacToe::TicTacToe()
{
    newGame();
}

void TicTacToe::newGame()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            board[i][j] = 0;
        }
    }

    cout << "Enter \n0 for Player vs Player\n1 for Player vs Computer\n: ";
    cin >> autoplay;
    srand(time(NULL));
}

void TicTacToe::startGame()
{
    player = 1;
    int nextMove;
    int count = 0;

    do
    {
        showBoard();
        if (!autoplay)
        {
            nextMove = getMove();
        }
        else
        {
            if (player == 1)
            {
                nextMove = getMove();
            }
            else
            {
                nextMove = compMove();
            }
        }

        if (isValidMove(nextMove))
        {
            makeMove(nextMove);
            count++;
        }
        else
        {
            cout << endl
                 << "------  P L E A S E  E N T E R  A  V A L I D  M O V E  -----" << endl;
            nextPlayer();
        }

        if ((count >= 9))
        {
            showBoard();
            cout << "-------   D R A W  !!    ----" << endl;
            return;
        }

    } while (!isPlayerWon());
    showBoard();
    cout << "----   P L A Y E R  " << player << "  W O N !!   ----" << endl;
    ;
}

int TicTacToe::getMove()
{
    int number;
    cout << "player " << player << " : ";
    cin >> number;
    return number;
}

int TicTacToe::compMove()
{
    int random;
    while (1)
    {
        random = (rand() % 9) + 1;
        if (isValidMove(random))
            return random;
    }
}

bool TicTacToe::isValidMove(int nextMove)
{
    if (nextMove >= 1 && nextMove <= 9)
    {
        if (!(board[label.row(nextMove)][label.col(nextMove)]))
        {
            return true;
        }
    }
    return false;
}

void TicTacToe::makeMove(int move)
{
    board[label.row(move)][label.col(move)] = player;
}

bool TicTacToe::isPlayerWon()
{
    //row
    if (boardStatus(1) == player && boardStatus(2) == player && boardStatus(3) == player)
        return true;

    if (boardStatus(4) == player && boardStatus(5) == player && boardStatus(6) == player)
        return true;

    if (boardStatus(7) == player && boardStatus(8) == player && boardStatus(9) == player)
        return true;

    //column
    if (boardStatus(1) == player && boardStatus(4) == player && boardStatus(7) == player)
        return true;

    if (boardStatus(2) == player && boardStatus(5) == player && boardStatus(8) == player)
        return true;

    if (boardStatus(3) == player && boardStatus(6) == player && boardStatus(9) == player)
        return true;

    //diagonal
    if (boardStatus(3) == player && boardStatus(5) == player && boardStatus(7) == player)
        return true;

    if (boardStatus(1) == player && boardStatus(5) == player && boardStatus(9) == player)
        return true;

    nextPlayer();
}

int TicTacToe::boardStatus(int n)
{
    return board[label.row(n)][label.col(n)];
}

void TicTacToe::nextPlayer()
{
    if (player == 1)
    {
        player = 2;
    }
    else
    {
        player = 1;
    }
}

void TicTacToe::showBoard()
{

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            cout << board[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl
         << endl;
}

void TicTacToe::showGuide()
{
    cout << "----  B o x    L a b e l   G u i d e  ----" << endl;
    for (int i = 0; i < 9; i++)
    {
        if (i % 3 == 0)
            cout << endl
                 << "\t\t";
        cout << i + 1 << " ";
    }
    cout << "\n\n------------------------------------------";
    cout << "\n----- Enter any number between 1 to 9 ----";
    cout << "\n------     To   S T A R T  playing   -----" << endl
         << endl;
}
