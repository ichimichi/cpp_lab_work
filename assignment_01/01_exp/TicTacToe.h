//Interface for TicTacToe class
#ifndef TICTACTOE_H
#define TICTACTOE_H

// Helper class, which maps the row and column index
// [0][0],[0][1],...,[2][2]
// to digits 1-9
class SquareLabel
{
  private:
    struct box
    {
        int i, j;
    };
    box squareNo[9];

  public:
    SquareLabel();
    //returns the appropriate row for label n
    int row(int n);
    //returns the appropriate column for label n
    int col(int n);
};

class TicTacToe
{
  private:
    int board[3][3];
    int player;
    int autoplay;   // set to 1 for P vs Comp , 0 for P vs P
    SquareLabel label; 
    // SquareLabel object which help retrieve row and column for label i (1-9)

  public:
    // constructor which calls newGame()
    TicTacToe();
    
    // configures the settings of the game and
    // initializes the board to 0s
    void newGame();

    // starts the game with player 1 to make the first move
    void startGame();

    // simply prompts the user for the next move
    // and returns the move selected
    int getMove();

    // Randomly generates a move and returns the move
    // if move is valid
    int compMove();

    // returns true if the nextMove is valid
    // i.e, between 1-9 and not selected already
    bool isValidMove(int nextMove);

    // simply assigns the player code to the corresponding
    // square on the board
    void makeMove(int move);

    // returns true if the game is WON by a player
    // other wise toggles between player 1 and 2
    bool isPlayerWon();

    // simply returns the number assigned to the square with
    // label n in the board, it can return 
    // 0 - square not yet selected
    // 1 - square selected by player 1
    // 2 - square selected by player 2
    int boardStatus(int n);

    // toggles between player 1 and 2
    void nextPlayer();

    // shows the graphical board on the terminal
    void showBoard();

    // shows the tutorial on how to play at the 
    // beginning of the game
    void showGuide();
};
  

#endif //TICTACTOE_H