#include<iostream>
using namespace std;

template <typename T>
class Array
{
    T arr[10];
    public:
    Array()
    {   
        cout<<"Array of elements each of size "<<sizeof(arr[0])<<" Bytes created"<<endl;
    }

};

template <>
class Array<float>
{
    float arr[10];
    public:
    Array()
    {   
        cout<<"Array of float elements each of size "<<sizeof(arr[0])<<" Bytes created"<<endl;
    }

};