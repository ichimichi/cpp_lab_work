#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cstdio>
using namespace std;

class Tool
{
  public:
    int id_no;
    char name[30];
    int quantity;
    double cost;

    Tool(int id = 0, string nm = "", int qnty = 0, double c = 0)
    {
        id_no = id;
        setName(nm);
        quantity = qnty;
        cost = c;
    }

    void setName(string nm)
    {
        size_t length = nm.size();
        length = length < 30 ? length : 29;
        nm.copy(name, length);
        name[length] = '\0';
    }
};

void createFile()
{
    fstream toolsFile("tools.dat", ios::out | ios::binary);

    if (!toolsFile)
    {
        cout << "File could not be opened." << endl;
        exit(1);
    }

    toolsFile.seekp(0);
    Tool temp;

    for (int i = 0; i < 100; i++)
    {
        toolsFile.write(reinterpret_cast<const char *>(&temp), sizeof(temp));
    }

    toolsFile.close();
}

void addRecord()
{
    fstream toolsFile("tools.dat", ios::in | ios::out | ios::binary);

    int id;
    string toolName;

    Tool temp;
    cout << "identification no. (1-100) : ";
    cin >> temp.id_no;
    cin.ignore();
    cout << "tool name : ";
    getline(cin, toolName);
    temp.setName(toolName);
    cout << "quantity : ";
    cin >> temp.quantity;
    cout << "cost : ";
    cin >> temp.cost;

    toolsFile.seekp((temp.id_no - 1) * sizeof(temp));

    toolsFile.write(reinterpret_cast<const char *>(&temp), sizeof(temp));

    toolsFile.close();
}

void updateRecord(int id)
{
    fstream toolsFile("tools.dat", ios::in | ios::out | ios::binary);

    string toolName;

    Tool temp;

    cin.ignore();
    cout << "tool name : ";
    getline(cin, toolName);
    temp.setName(toolName);
    cout << "quantity : ";
    cin >> temp.quantity;
    cout << "cost : ";
    cin >> temp.cost;

    

    toolsFile.seekp((temp.id_no - 1) * sizeof(temp));

    toolsFile.write(reinterpret_cast<const char *>(&temp), sizeof(temp));

    toolsFile.close();
}

void displayRecord()
{
    fstream toolsFile("tools.dat", ios::in | ios::binary);

    if (!toolsFile)
    {
        cout << "File could not be opened." << endl;
        exit(1);
    }
    Tool temp;
    toolsFile.read(reinterpret_cast<char *>(&temp), sizeof(temp));
    cout << left << endl
         << endl
         << setw(8) << "Rec. #" << setw(22) << "Tool Name" << setw(10) << "Quantity" << setw(7) << "Cost" << endl;
    while (toolsFile)
    {
        if (temp.id_no != 0)
        {
            cout << setw(8) << temp.id_no << setw(22) << temp.name << setw(10) << temp.quantity << setw(7) << setprecision(2) << fixed << showpoint << temp.cost << endl;
        }
        toolsFile.read(reinterpret_cast<char *>(&temp), sizeof(temp));
    }

    cout << "-------------------------------------------------------------------------------------" << endl
         << endl;
    toolsFile.close();
}

void deleteRecord(int id)
{
    fstream toolsFile("tools.dat",ios::in | ios::out | ios::binary);

    Tool temp;

    toolsFile.seekp((id - 1) * sizeof(temp));

    toolsFile.write(reinterpret_cast<const char *>(&temp), sizeof(temp));

    toolsFile.close();
}
