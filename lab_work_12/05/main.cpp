#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cstdio>
#include "Inventory.cpp"
using namespace std;

int main()
{
    int id;
    char choice;
    cout << "Do you wish to create a new file (y/n): ";
    cin >> choice;

    if (tolower(choice) == 'y')
    {
        createFile();
    }

    while (true)
    {
        displayRecord();
        cout << "Menu" << endl;
        cout << "1. Add Record" << endl;
        cout << "2. Delete Record" << endl;
        cout << "3. Update Record" << endl;
        cout << "4. Delete all Records" << endl;
        cout << "5. Display Records" << endl;
        cout << "0. Exit" << endl;
        cout << "Enter choice : ";
        cin >> choice;
        switch (choice)
        {
        case '1':
            addRecord();
            break;
        case '2':
            cout << "Enter Record Number : ";
            cin >> id;
            deleteRecord(id);
            cout << "Record " << id << " deleted." << endl;
            break;
        case '3':
            cout << "Enter Record Number : ";
            cin >> id;
            updateRecord(id);
            cout << "Record " << id << " updated." << endl;
            break;
        case '4':
            createFile();
            cout << "All Records deleted." << endl;
            break;
        case '5':
            displayRecord();
            break;
        case '0':
            exit(0);
        }
    }

    return 0;
}