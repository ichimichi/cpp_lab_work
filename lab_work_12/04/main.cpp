#include <iostream>
#include <exception>
#include <cmath>
#include <bitset>
#include <vector>
using namespace std;

int main()
{
    try
    {
        throw 0; // integer exception
    }
    catch (...)
    {
        cout << "Integer exception" << endl;
    }

    try
    {
        throw 'A'; // character exception
    }
    catch (...)
    {
        cout << "Character exception" << endl;
    }

    try
    {
        throw 1.0; // double exception
    }
    catch (...)
    {
        cout << "Double exception" << endl;
    }

    try
    {
        int *myarray = new int[100000000000000000000000];
    }
    catch (...)
    {
        cout << "bad_alloc Exception" << endl;
    }

    try
    {
        int num = -10;
        if (num < 0)
        {
            throw new domain_error("square root of negative number");
        }
        int sq_root_of_num = sqrt(num);
        cout << sq_root_of_num;
    }
    catch (...)
    {
        cout << "domain_error Exception" << endl;
    }

    try
    {
        // bitset constructor throws an invalid_argument if initialized
        // with a string containing characters other than 0 and 1
        bitset<5> mybitset("01234");
    }
    catch (...)
    {
        cout << "invalid_argument exception " << endl;
    }

    try
    {
        // vector throws a length_error if resized above max_size
        vector<int> myvector;
        myvector.resize(myvector.max_size() + 1);
    }
    catch (...)
    {
        cout << "length_error exception" << endl;
    }

    vector<int> myvector(10);
    try
    {
        myvector.at(20) = 100; // vector::at() throws an out-of-range
    }
    catch (const std::out_of_range &oor)
    {
        cout << "out_of_range exception" <<endl;
    }

    try
    {
        bitset<65> mybitset("11111111111111111111111111111111111111111111111111111111111111111");
        // throws overflow_error if the value can not be represented in unsigned long
        // and to do so any bitset more than 64 bits long will cause it since 
        // usigned long takes 8 bytes of memory (2^8 = 64 , max number of bits)
        cout<<mybitset.to_ulong()<<endl;
    }
    catch(...)
    {
        cout<< "overflow_error exception"<<endl;
    }
}