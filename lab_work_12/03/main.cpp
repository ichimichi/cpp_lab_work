#include<iostream>
using namespace std;

class A
{   
    int objectNo;
    
    public:
    static int count;
    A()
    {   
        objectNo = ++count;
        cout<<endl<<"Object "<<objectNo<<" created";
    }

    ~A()
    {
        cout<<endl<<"Object "<<objectNo<<" destroyed";
    }
};

int A::count = 0;

int main()
{
    A a1;
    A a2;
    int fakeException = 0;
    try
    {
        if(fakeException ==0)
        {
            throw 0;
        }
        A a4;
    }
    catch(int)
    {
        cerr<<endl<<"Exception occured";
    }

    
    return 0;
}