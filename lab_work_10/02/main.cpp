#include<iostream>
#include"Time.cpp" //including the source file of our Time class

using namespace std;

int main()
{   
    Time t1(2000); //declaring object t1 of Time class

    t1.dispTimeStd();
    //pre incrementing the time by 1 second
    cout<<++t1;
    cout<<t1;

    cout<<endl;

    Time t2(2000);
    t2.dispTimeStd();
    //post incrementing the time by 1 second
    cout<<t2++;
    cout<<t2;


    return 0;
}


