#include <iostream>
#include <iomanip> //included for setfill() and setw() functions which are used for displaying
                   //preceding zeros, e.g, 09:04:00 PM
using namespace std;

class Time
{
  private:
    int timeElapsed;

  public:
    //parameterised constructor which takes in the seconds elapsed
    //and calls the setTime() method
    Time(int x)
    {
        setTime(x);
    }

    //default constructor
    Time()
    {
        timeElapsed = 0;
    }

    //set method which takes in the seconds elapsed
    void setTime(int x)
    {
        // timeElapsed can't initially be more than 24hours
        if (x > 86400)
        {
            cout << "\n>invalid entry."; //24 hours = 86400 seconds
        }
        else
        {
            timeElapsed = x;
        }
    }

    //displays the time in 24-hour format or Universal format or Military format
    void dispTimeUni()
    {
        cout << setfill('0') << setw(2) << getHour() << ":" << setw(2) << getMinutes() - getHour() * 60 << ":" << setw(2) << getSeconds() << endl;
    }

    //displays the time in 12-hour format with AM and PM
    void dispTimeStd()
    {
        if (getHour() >= 12)
        {
            cout << setfill('0') << setw(2) << (getHour() % 12 == 0 ? 12 : getHour() % 12) << ":" << setw(2) << getMinutes() - getHour() * 60 << ":" << setw(2) << getSeconds() << " PM" << endl;
        }
        else
        {
            cout << setfill('0') << setw(2) << (getHour() % 12 == 0 ? 12 : getHour() % 12) << ":" << setw(2) << getMinutes() - getHour() * 60 << ":" << setw(2) << getSeconds() << " AM" << endl;
        }
    }

    //tick() method function that increments the time stored in a Time object by one second
    void tick()
    {
        timeElapsed++;
        dispTimeStd();
    }

    //returns the number of hours elapsed in a day
    int getHour() const
    {
        return (timeElapsed % 86400) / (60 * 60);
    }

    //returns the number of minutes elapsed in an hour
    int getMinutes() const
    {
        return (timeElapsed % 86400) / 60;
    }

    //returns the number of seconds elapsed in a minute
    int getSeconds() const
    {
        return (timeElapsed % 86400) - getMinutes() * 60;
    }

    // overloading the pre fix ++ operator to increment the
    // the time elapsed by 1 second and return the modified object
    Time operator++()
    {
        timeElapsed++;
        return *this;
    }

    // overloading the post fix ++ operator to return the un-modified
    // object and the inrement the time elapsed by 1 second 
    Time operator++(int)
    {
        Time temp = *this;
        timeElapsed++;
        return temp;
    }

    // overloading the stream insertion << operator to display the time
    // in Standard format
    friend ostream &operator<<(ostream &out,const Time &t){
        if (t.getHour() >= 12)
        {
            out << setfill('0') << setw(2) << (t.getHour() % 12 == 0 ? 12 : t.getHour() % 12) << ":" << setw(2) << t.getMinutes() - t.getHour() * 60 << ":" << setw(2) << t.getSeconds() << " PM" << endl;
        }
        else
        {
            out << setfill('0') << setw(2) << (t.getHour() % 12 == 0 ? 12 : t.getHour() % 12) << ":" << setw(2) << t.getMinutes() - t.getHour() * 60 << ":" << setw(2) << t.getSeconds() << " AM" << endl;
        }
        return out;
    }
};
