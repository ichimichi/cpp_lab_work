#include <string>
#include <iostream>
using namespace std;


class PhoneNumber
{
  private:
    int std;
    int number;

  public:
    PhoneNumber(long long num = 0)
    {
        set(num);
    }

    // separates the phone number into std code and number
    void set(long long num)
    {
        number = num % 10000000;
        std = num / 10000000;
    }

    void get()
    {
        cout << std << " " << number;
    }

    // overloading the stream insertion operator <<
    friend ostream &operator<<(ostream &out, const PhoneNumber &p);

    // overloading the stream extraction operator >>
    friend istream &operator>>(istream &input, PhoneNumber &p);
};

ostream &operator<<(ostream &out, const PhoneNumber &p)
{
    out << p.std << "-" << p.number;
    return out;
}

istream &operator>>(istream &input, PhoneNumber &p)
{
    long long num;
    input >> num;
    p.set(num);
    return input;
}
