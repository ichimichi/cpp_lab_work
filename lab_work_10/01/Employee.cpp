#include <stdio.h>
#include "Date.cpp"
#include "PhoneNumber.cpp"
using namespace std;

class Employee
{
  public:
    string empName;
    Date join;
    PhoneNumber number;

  public:
    Employee(string name = "null", string doj = "12/12/1212", long long num = 9999999999)
    {
        empName = name;
        join.set(doj);
        number.set(num);
    }

    // overloading the stream insertion operator <<
    friend ostream &operator<<(ostream &out, const Employee &emp);

    // overloading the stream extraction operator >>
    friend istream &operator>>(istream &input, Employee &emp);
};

ostream &operator<<(ostream &out, const Employee &emp)
{
    out << emp.empName << " " << emp.join << " " << emp.number;
    return out;
}

istream &operator>>(istream &input, Employee &emp)
{
    input >> emp.empName >> emp.join >> emp.number;
    return input;
}