#include<string>
#include<iostream>

#include "Employee.cpp"

using namespace std;

int main(){
    Employee emp;

    cout<<"Enter details (separated by space)\n";
    cout<<"<name> <date of joining mm/dd/yyyy> <phone number with std code>\n";
    cout<<"For Example, \n: Lari 12/12/2012 03642535304\n\n: ";
    
    cin>>emp;

    cout<<emp<<endl;

    return 0;
}