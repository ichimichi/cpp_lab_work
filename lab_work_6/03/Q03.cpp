#include <iostream>
#include <string>
#include "Date.cpp" //including the source file of our Date class
using namespace std;

int main()
{
    Date d1;            //declaring an object d1 of Date
    string input;       //variable input will be used to take the date input from the user and
                        //set it to our Date object d1 using the set() method defined in the class

    cout << "Enter date (mm/dd/yyyy): ";//prompt to user so that the user knows that an input has to be entered in the
                                        //specified format such as 09/04/1997
    getline(cin, input);    //stores characters from the cin stream and stores them in string variable input until a newline
                            //character '\n' is encountered
    d1.set(input);          //the string input is passed to the set() method , which gets evaulated on being a valid date
                            //before accepting it

    cout << "\nYou entered : ";
    d1.dispFormat2();       //dispFormat2() method diplays a date in a similar format as below 
                            //September 4, 1997
    return 0;
}
