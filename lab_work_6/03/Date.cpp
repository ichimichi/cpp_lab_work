#include <iostream>
#include <string>
#include <iomanip>  //included for setfill() and setw() functions which are used for displaying
                    //preceding zeros, e.g, 09/04/1997
using namespace std;

//Date class which will store the user input date and display in any of the two formats
//09/04/1997 or September 4, 1997
class Date
{   //private variables for storing the date, month and year respectively
  private:
    int dd;
    int mm;
    int yyyy;

  public:
    Date()  //default constructor, required for initial declaration
    {
        dd = 0;
        mm = 0;
        yyyy = 0;
    }

    Date(string inputDate)  //constructor recieves the inputDate entered by the user and decodes
    {                       //the corresponding date,month and year from string inputDate
        int m = 0;          //initializing m,d,y and x to 0
        int d = 0;          //m,d,y will be storing the month, date and year upon calculation
        int y = 0;
        int x = 0;          // x is used to help determine in which part of the date the program is in
                            // x = 0, implies that the program is in the mm segment
                            // x = 1, implies that the program is in the dd segment
                            // x = 2, implies that the program us in the yyyy segment

        //for each for loop / advanced for loop which goes through each character in the string inputDate
        //and storing it in char variable c
        for (char c : inputDate)
        {
            if (c == '/')   //on evaluating true the program will know that the previous segment has ended and 
            {               //will increment x by 1, and let itself know that the next segment will be staring in the next
                x++;        //iteration
                continue;   //skips the rest of the block and starts iteration for the next character
            }

            //extracts the month from the inputDate and stores it in m
            if (c != 0 && x == 0)
            {
                m = m * 10 + (c - '0');
            }

            //extracts the date from the inputDate and stores it in d
            if (c != 0 && x == 1)
            {
                d = d * 10 + (c - '0');
            }

            //extracts the year from the inputDate and stores it in y
            if (c != 0 && x == 2)
            {
                y = y * 10 + (c - '0');
            }
        }

        //checking whether the date entered by the user is a valid one or not
        //before assigning it to the object's mm,dd and yyyy variables
        if (checkValidity(m, d, y) == true) //on evaluating to false the user will be prompted about 
        {                                   //the invalid input and the object's variables won't be modified
            mm = m;
            dd = d;
            yyyy = y;
        }
    }
    

    //setter method for our Date class, it's function is very much alike the constructor above
    void set(string inputDate)
    {
        int m = 0;
        int d = 0;
        int y = 0;
        int x = 0;
        for (char c : inputDate)
        {
            if (c == '/')
            {
                x++;
                continue;
            }
            if (c != 0 && x == 0)
            {
                m = m * 10 + (c - '0');
            }
            if (c != 0 && x == 1)
            {
                d = d * 10 + (c - '0');
            }
            if (c != 0 && x == 2)
            {
                y = y * 10 + (c - '0');
            }
        }

        if (checkValidity(m, d, y) == true)
        {
            mm = m;
            dd = d;
            yyyy = y;
        }
    }

    
    //second format of displaying the date
    void dispFormat1()
    {
        cout << setfill('0') << setw(2) << mm << "/";
        cout << setfill('0') << setw(2) << dd << "/";
        cout << setfill('0') << setw(4) << yyyy << endl;
    }

    //second format of displaying the date
    void dispFormat2()
    {   //switch case for mapping the numerical month to the corresponding textual month
        switch (mm)
        {
        case 1:
            cout << "January ";
            break;
        case 2:
            cout << "February ";
            break;
        case 3:
            cout << "March ";
            break;
        case 4:
            cout << "April ";
            break;
        case 5:
            cout << "May ";
            break;
        case 6:
            cout << "June ";
            break;
        case 7:
            cout << "July ";
            break;
        case 8:
            cout << "August ";
            break;
        case 9:
            cout << "September ";
            break;
        case 10:
            cout << "October ";
            break;
        case 11:
            cout << "Novemver ";
            break;
        case 12:
            cout << "December ";
            break;
        }

        cout << dd << ", ";
        cout << yyyy << endl;
    }

    //check for validity of the specified date
    //returns true if it is valid and false if it is invalid
    bool checkValidity(int m, int d, int y)
    {
        if (!(m >= 1 && m <= 12))
        {
            cout << m << " is not a valid month.\n";
            return false;
        }

        if (!(d >= 1 && d <= 31))
        {
            cout << d << " is not a date.\n";
            return false;
        }

        return true;
    }
};