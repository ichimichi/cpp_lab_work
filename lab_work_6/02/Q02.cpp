#include <iostream>
#include <string>
#include "CustomString.cpp" //including the source file of our CustomString class
using namespace std;

int main(){
    CustomString str;   //declaring an object str of CustomString
    string input;       //variable input will be used to take the string input from the user and
                        //set it to our CustomString object str using the set() method defined in the class
    cout<<"Enter a line: ";//prompt to user so that the user knows that an input has to be entered
    getline(cin,input); //stores characters from the cin stream and stores them in string variable input until a newline
                        //character '\n' is encountered
    str.set(input);     //the string input is passed to the set() method , which sets the private varialble str
                        //to input, which the user has entered

    //getWordLengthOccurences() method is called on CustomString object str
    //which will calculate the number of occurences of various word length in str which has been set to the input,
    //which is entered by the user, in a tablular format
    str.getWordLengthOccurences();

    return 0;
}