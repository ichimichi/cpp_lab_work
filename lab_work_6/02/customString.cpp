#include <string>
#include <iostream>
using namespace std;

//class to create a string object with our custom methods
class CustomString
{
  private:
    string str;//str will contain the string entered by the user

  public:
    CustomString(const string x)
    {
        str = x; //constructor sets the value of string str to x
    }

    CustomString()//default constructor , required for initial declaration
    {
    }

    void set(const string x)
    {
        str = x;//setter for CustomString object, enable user to change the input string
    }

    string get()
    {
        return str;//getter for CustomString object , returns the value of str
    }

    //method to calculate the frequency of alphabets in the string str of the object
    void getAlphabetOccurences()
    {   //the count of each alphabet is mapped to an element of array freq[]
        //freq[0] gives the frequency of alphabet 'a'
        //freq[1] gives the frequency of alphabet 'b' and similarly for the rest of the alphabets
        int freq[26] = {0};//initializing the count of each aphabet to zero

        //for each for loop / advanced for loop which goes through each character in the string
        //staring from the first one and storing it in c and on the next iteration of the loop
        //the next characterwill be stored in variable c and so on.
        for (char c : str)
        {
            c = tolower(c);//converting each character to lowercase
            if (c >= 'a' && c <= 'z') //if the character is and alphabet ,i.e, ACSII value lies between 
                freq[c - 'a']++;//97 and (97+25) then increment the frequency of that character by 1
        }

        //displaying the occurence of each alphabet in a tabular format
        cout << "alphabet\toccurences" << endl;
        for (int i = 0; i < 26; i++)
        {
                cout << (char)(i + 97) << "\t\t" << freq[i] << endl;
        }
        
    }

    //method to calculate the occurences/frequencies of various word length in str
    void getWordLengthOccurences()
    {   //word[i] gives the count of each word length i
        //word[1] gives the number of words with length of 1
        //word[3] gives the number of words with length of 3 and so on
        int word[46] = {0}; //index set to 46 , since the longest possible word
                            //in english dictionary is 45


        int length=0;   //length is used to store the word length of the current word in each iteration

        //a for each loop which goes through each character in string str one at a time in each
        //iteration and store it in char variable c 
        for (char c : str)
        {
            if (c != ' '){  //until a space has been encountered, the word is still incomplete 
                length++;   //therefore we need increment the length of the current word by 1
            }
            else            //when a space is encountered, we know that the current word is complete
            {               //and the next character will be the first character of the new word
                word[length]++; //therefore we increment the count of the word with length characters
                length=0;       //and reinitialize the length variable to zero to be reused for the next word in str
            }
        }
        //since the last character in the string is a null character the above for loop won't take it into consideration
        //there for need the additional statement below
        word[length]++; //this statement is used to increment the count of the word length of the last word in str 

        //displaying the occurence of various word lengths which are greater than 0 in str in a tabular format
        cout << "word length\toccurences" << endl;
        for (int i = 1; i < 46; i++)
        {
                cout << i << "\t\t" << word[i] << endl;
        }
        
    }
};