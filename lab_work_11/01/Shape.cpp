#include <iostream>
#include <cmath>
using namespace std;
class Shape
{
  public:
    virtual double getArea() = 0;
    virtual double getVolume()
    {
    }
};

class TwoDimensionalShape : public Shape
{

};

class ThreeDimensionalShape : virtual public Shape
{

};

class Circle : public TwoDimensionalShape
{
    double radius;

  public:
    Circle(double r)
    {
        setRadius(r);
    }
    void setRadius(double r)
    {
        radius = r;
    }
    double getArea()
    {
        return 2 * 3.1415 * radius * radius;
    }
};

class Square : public TwoDimensionalShape
{
    double side;

  public:
    Square(double s)
    {
        setSide(s);
    }
    void setSide(double s)
    {
        side = s;
    }
    double getArea()
    {
        return side * side;
    }
};

class Triangle : public TwoDimensionalShape
{
    double side1, side2, side3;

  public:
    Triangle(double s1, double s2, double s3)
    {
        setSides(s1, s2, s3);
    }
    void setSides(double s1, double s2, double s3)
    {
        side1 = s1;
        side2 = s2;
        side3 = s3;
    }
    double getArea()
    {
        double s = (side1 + side2 + side3) / 2;
        return sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }
};

class Sphere : public ThreeDimensionalShape
{
    double radius;

  public:
    Sphere(double r)
    {
        setRadius(r);
    }
    void setRadius(double r)
    {
        radius = r;
    }
    double getArea()
    {
        return 4 * 3.1415 * radius * radius;
    }

    double getVolume()
    {
        return (4.0 / 3.0) * 3.1415 * radius * radius * radius;
    }
};

class Cube : public ThreeDimensionalShape
{
    double side;

  public:
    Cube(double s)
    {
        side = s;
    }
    double getArea()
    {
        return 6 * side * side;
    }

    double getVolume()
    {
        return side * side * side;
    }
};

//Regular Tetrahedron
class Tetrahedron : public ThreeDimensionalShape
{
    double edge;

  public:
    Tetrahedron(double e)
    {
        edge = e;
    }
    double getArea()
    {
        return sqrt(3) * edge * edge;
    }

    double getVolume()
    {
        return (edge * edge * edge) / (6 * sqrt(2));
    }
};
