#include <iostream>
#include "Shape.cpp"
using namespace std;

int main()
{
    Shape *shapePtr[6];
    Circle c(10);
    Square s(10);
    Triangle t(10, 10, 10);
    Sphere sp(10);
    Cube cb(10);
    Tetrahedron tetr(10);
    shapePtr[0] = &c;
    shapePtr[1] = &s;
    shapePtr[2] = &t;
    shapePtr[3] = &sp;
    shapePtr[4] = &cb;
    shapePtr[5] = &tetr;

    cout << endl
         << "Circle:" << endl;
    cout << shapePtr[0]->getArea() << endl;

    cout << endl
         << "Square:" << endl;
    cout << shapePtr[1]->getArea() << endl;

    cout << endl
         << "Triangle:" << endl;
    cout << shapePtr[2]->getArea() << endl;

    cout << endl
         << "Sphere:" << endl;
    cout << shapePtr[3]->getArea() << endl;
    cout << shapePtr[3]->getVolume() << endl;

    cout << endl
         << "Cube:" << endl;
    cout << shapePtr[4]->getArea() << endl;
    cout << shapePtr[4]->getVolume() << endl;

    cout << endl
         << "Tetrahedron:" << endl;
    cout << shapePtr[5]->getArea() << endl;
    cout << shapePtr[5]->getVolume() << endl;
}