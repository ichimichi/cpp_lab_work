#include <iostream>
#include <string>
using namespace std;

// Shipping Company class which contains it's Shipping Company Charges
class ShippingCompany
{
    public:
    string shippingCompanyName;
    double shippingCompanyCharges;
};

// Base class
class Package
{
  public:
    // 0 - stores Recipient Details
    // 1 - stores Sender Details
    string name[2];
    string address[2];
    string city[2];
    string state[2];
    string zip[2];
    double weight;
    double costPerKg;
    


  public:
    virtual void getRecipient() =0 ;
    virtual void getSender () = 0 ;
    // constructor
    // sets data members to 0 or "" unless specified during
    // object creation
    Package(double weight = 0.0, double costPerKg = 0.0, string rName = "", string rAddress = "", string rCity = "", string rState = "", string rZip = "", string sName = "", string sAddress = "", string sCity = "", string sState = "", string sZip = "")
    {
        setRecipient(rName, rAddress, rCity, rState, rZip);
        setSender(sName, sAddress, sCity, sState, sZip);
        setWeight(weight);
        setCostPerKg(costPerKg);
    }

    // sets the information of the recipient
    void setRecipient(string rName, string rAddress, string rCity, string rState, string rZip)
    {
        name[0] = rName;
        address[0] = rAddress;
        city[0] = rCity;
        state[0] = rState;
        zip[0] = rZip;
    }

    // sets the information of the sender
    void setSender(string sName, string sAddress, string sCity, string sState, string sZip)
    {
        name[1] = sName;
        address[1] = sAddress;
        city[1] = sCity;
        state[1] = sState;
        zip[1] = sZip;
    }

    // sets the weight of the package in kg unless weight entered is 
    // negative
    void setWeight(double w)
    {
        if (w >= 0)
        {
            weight = w;
        }
        else
        {
            cout << "Weight cannot be negative" << endl;
        }
    }

    // sets the cost per kg of the package
    // unless the cost per kg entered is negative
    void setCostPerKg(double cpg)
    {
        if (cpg >= 0)
        {
            costPerKg = cpg;
        }
        else
        {
            cout << "Cost per Kg cannot be negative" << endl;
        }
    }

    // returns the weight of the package in kg
    double getWeight(){
        return weight;
    }

    // returns the costPerKg value
    double getCostPerKg(){
        return costPerKg;
    }

    // returns the cost of the package for shipping
    virtual double calculateCost()
    {
        return weight * costPerKg;
    }
};

// Derived class: Multiple inheritance from class Package and
// class ShippingCompany
class TwoDayPackage : public Package, public ShippingCompany
{

  public:
    // TwoDayPackage’s constructor receives a value to 
    // initialize this data member shippingCompanyCharges of
    // ShippingCompany class
    TwoDayPackage(double charges)
    {
        shippingCompanyCharges = charges;
    }

    // TwodayPackage redefines member function CalculateCost() so that it computes the 
    // shipping cost by adding the shippingCompanyCharges to the 
    // weight-based cost calculated by base class package’s CalculateCost() function
    double calculateCost()
    {
        return Package::calculateCost() + shippingCompanyCharges;
    }

    virtual void getRecipient()
    {
        cout<<name[0]<<", "<<address[0]<<", "<<city[0]<<"-"<<zip[0]<<", "<<state[0]<<endl;
    }
    virtual void getSender ()
    {
        cout<<name[1]<<", "<<address[1]<<", "<<city[1]<<"-"<<zip[1]<<", "<<state[1]<<endl;
    }
};

// Derived class : inherits directly from
// class Package
class OvernightPackage : public Package
{
  private:
    // additional data memeber representing additional fee per kg
    // charged for overnight delivery service 
    double additionalFeePerKg;
    
  public:
    OvernightPackage(double FeePerKg)
    {
        additionalFeePerKg = FeePerKg;
    }

    // OvernightPackage redefines member function CalculateCost so that it adds the additional 
    // fee per kg to the standard cost per kg before calculating the shipping cost
    double calculateCost(){
        Package::setCostPerKg(getCostPerKg() + additionalFeePerKg);
        return Package::calculateCost();
    }

    virtual void getRecipient()
    {
        cout<<name[0]<<", "<<address[0]<<", "<<city[0]<<"-"<<zip[0]<<", "<<state[0]<<endl;
    }
    virtual void getSender ()
    {
        cout<<name[1]<<", "<<address[1]<<", "<<city[1]<<"-"<<zip[1]<<", "<<state[1]<<endl;
    }
};