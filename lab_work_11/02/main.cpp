// Driver Program

#include <iostream>
using namespace std;
#include "Package.cpp"

int main(){
    Package *packagePtr[2];
    double cost;
    double totalShippingCost = 0;

    TwoDayPackage package1(49.9);
    package1.setSender("Laribok","Mawrie Complex, Rynjah","Shillong","Meghalaya","793006");
    package1.setRecipient("Ichimichi","Greater Noida Expressway, Sector 135","Noida","Uttar Pradesh","201301");
    package1.setWeight(15.2);
    package1.setCostPerKg(499.9);


    OvernightPackage package2(49.9);
    package2.setSender("Rilangki","General Hospital, Sector 135","Noida","Uttar Pradesh","201301");
    package2.setRecipient("Gaurav","Post Office, Rynjah","Shillong","Meghalaya","793006");
    package2.setWeight(15.2);
    package2.setCostPerKg(499.9);


    packagePtr[0] = &package1;
    packagePtr[1] = &package2;

    for(int i=0;i<2;i++)
    {   cout<<"Package No. : "<<i+1<<endl;
        cout<<"To,"<<endl;
        packagePtr[i]->getRecipient();
        cout<<"From,"<<endl;
        packagePtr[i]->getSender();
        cost = packagePtr[i]->calculateCost();
        cout<<"**cost : "<<cost;
        cout<<endl<<"********************************************************************************"<<endl;
        totalShippingCost+=cost;
        
    }
    cout<<"****Total Shipping Cost : "<<totalShippingCost;


    return 0;
}