function overloading

Create a class Rational for performing arithmetic operations on fractions

1. Use integer variables to represent private data of class that can be numerator and denominator
2. Provide a constructor for initializing the objects . It should contain default values in case no initializers are provided and should 
store the fraction in reduced form. 
    Example:- 2/4 => 1/2 ....the numerator 1 and the denominator 2
3. Provide public member functions for following operations:-
    1. Adding two Rational numbers 
    2. Subtracting two Rational numbers
    3. Multiplying two Rational numbers
    4. Dividing two Rational numbers
    5. Printing Rational number in a/b form
    6. Printing Rational number in floating point form 

4. Only logically correct operation should be allowed. 