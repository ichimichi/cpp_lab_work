#include <iostream>
#include "rational.cpp"

using namespace std;


int main(){
  int a,b;

  system("cls");
  cout<<"R1 :";cin>>a;system("cls");
  cout<<"R1 :"<<a<<"/";cin>>b;
  Rational x(a,b);
  system("cls");
  cout<<"R2 :";cin>>a;system("cls");
  cout<<"R2 :"<<a<<"/";cin>>b;
  Rational y(a,b);
  system("cls");

  Rational result;
  cout<<"\t\t\ta/b\tfloating\n-------------------------------------------------\n";
  cout<<"\nR1\t\t:\t"; x.displayabform();
  cout<<"\t";x.displaypointform();
  cout<<"\nR2\t\t:\t"; y.displayabform();
  cout<<"\t";y.displaypointform();

  result.addRational(x,y);
  cout<<"\nADDITION\t:\t";result.displayabform();
  cout<<"\t";result.displaypointform();
  result.subRational(x,y);

  cout<<"\nSUBTRACTION\t:\t";result.displayabform();
  cout<<"\t";result.displaypointform();

  result.mulRational(x,y);
  cout<<"\nMULTIPLICATION\t:\t";result.displayabform();
  cout<<"\t";result.displaypointform();

  result.divRational(x,y);
  cout<<"\nDIVISION\t:\t";result.displayabform();
  cout<<"\t";result.displaypointform();

  return 0;
}