#include <iostream>

using namespace std;

class Rational
{
    private: 
    int numerator;
    int denominator;
    public:

    Rational(){
        numerator = 0;
        denominator = 0;
    }

    Rational(int n,int d){
        if(d==0){
            cout << "Object Creation failed , denominator can't be zero\n" << endl;
        }
        
        numerator = n;
        denominator = (numerator==0)?1 : d;

        Rational::reduce();
    }


    void addRational(Rational x1,Rational x2){
        if(x1.numerator==0 || x2.numerator ==0){
            if(x1.numerator != 0){
                numerator =x1.numerator;
                denominator = x1.denominator;
            }
            else{
                numerator =x2.numerator;
                denominator = x2.denominator;                
            }
        }
        else{
            if( x1.denominator!=x2.denominator ){

                denominator = lcm(x1.denominator,x2.denominator);
                int c= (denominator/x1.numerator);
                int d= (denominator/x2.numerator);
                
                if(x2.denominator==1){
                    d = x1.denominator;c=1;
                }
                if(x1.denominator==1){
                    c = x2.denominator;d=1;
                }
                numerator=x1.numerator * c + x2.numerator*d;

            }else{
                denominator = x1.denominator;
                numerator=x1.numerator  + x2.numerator;
            }
        }
        Rational::reduce();
    }


    void subRational(Rational x1,Rational x2){
        if(x1.numerator==0 || x2.numerator ==0){
            if(x1.numerator != 0){
                numerator =x1.numerator;
                denominator = x1.denominator;
            }
            else{
                numerator = -x2.numerator;
                denominator = x2.denominator;                
            }
        }
        else{
            if( x1.denominator!=x2.denominator ){

                denominator = lcm(x1.denominator,x2.denominator);
                int c= (denominator/x1.numerator);
                int d= (denominator/x2.numerator);

                if(x2.denominator==1){
                    d = x1.denominator;c=1;
                }
                if(x1.denominator==1){
                    c = x2.denominator;d=1;
                }
                
                numerator=x1.numerator * c - x2.numerator*d;

            }else{
                denominator = x1.denominator;
                numerator=x1.numerator  - x2.numerator;
            }
        }
        Rational::reduce();
    }


    void mulRational(Rational x1,Rational x2){
        numerator = x1.numerator * x2.numerator;
        denominator = x1.denominator * x2.denominator;
        Rational::reduce();
    }


    void divRational(Rational x1,Rational x2){

        numerator = x1.numerator * x2.denominator;
        denominator = x1.denominator * x2.numerator;
        Rational::reduce();
        
    }


    Rational reduce(){
        int k;
        if((k=gcd(numerator,denominator))!=1 && numerator!=0){
            if(k>0){
                numerator= numerator/k;
                denominator =denominator/k;
            }else{
                numerator= -numerator/k;
                denominator = -denominator/k;
            }
        }
    }

    int lcm(int n1, int n2)
    {   
        int max = (n1 > n2) ? n1 : n2;
        do
        {
            if (max % n1 == 0 && max % n2 == 0)
            {
                return max;
                break;
            }
            else
            ++max;
        } while (true);

    }

    int gcd(int a, int b)
    {
        if (b == 0)
            return a;

        gcd(b, a % b);
    }

    void displayabform(){
        if(denominator == 0){
            cout<<"NaN";
        
        }
        else if(numerator !=0)
            cout << numerator <<"/"<< denominator;
        else
            cout << 0;
    }

    void displaypointform(){
                if(denominator == 0){
            cout<<"NaN";
        
        }
        else if(numerator !=0)
            cout << (float)numerator /(float) denominator;
        else
            cout << 0;
    }

};