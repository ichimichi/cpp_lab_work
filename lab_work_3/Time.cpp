#include <iomanip>

class Time
{
private:
    int timeElapsed;
public:
    Time(int x){
        if(x>86400){
            cout<<"\n>invalid entry.";
        }
        else{
            if(x==86400) x=0;
        timeElapsed = x ;
        }
        cout<<"\n~~~~~~~~~~ Constructed Time object with "<<timeElapsed<<"s time elapsed";
    }

    Time(){
        timeElapsed = 0 ;
        cout<<"\n~~~~~~~~~~ Constructed Time object with "<<timeElapsed<<"s time elapsed";
    }


    ~Time(){
        cout<<"\n************************** Destroyed Time object with "<<timeElapsed<<"s time elapsed";
    }




    void setTime(int x){
        if(x>86400){
            cout<<"\n>invalid entry.";
        }
        else{
            if(x==86400)
                x=0;
        timeElapsed = x ;
        }
    }

    void dispTimeUni(){
        cout<<setfill ('0')<<setw(2)<<getHour()<<":"<<setw(2)<<getMinutes()-getHour()*60<<":"<<setw(2)<<timeElapsed-getMinutes()*60<<endl;

    }

    void dispTimeStd(){
        if(getHour()>=12){
                cout<<setfill ('0')<<setw(2)<<(getHour()%12==0?12:getHour()%12)<<":"<<setw(2)<<getMinutes()-getHour()*60<<":"<<setw(2)<<timeElapsed-getMinutes()*60<<" PM"<<endl;
        }
        else
        {
                cout<<setfill ('0')<<setw(2)<<getHour()<<":"<<setw(2)<<getMinutes()-getHour()*60<<":"<<setw(2)<<timeElapsed-getMinutes()*60<<" AM"<<endl;
        }


    }

    int getHour(){
        return timeElapsed/(60*60);
    }
    int getMinutes(){
        return timeElapsed/60;
    }


};
