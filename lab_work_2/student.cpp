#define ND "not defined"
#define NDI 99999
using namespace std;
class Student
{
public:
    Student(){
        studentName=ND;
        studentRollNo=NDI;
        studentDepartment=ND;
        studentYear=NDI;
    }

    Student(string name, int rollNo, string department,int year){
        studentName=name;
        studentRollNo=rollNo;
        studentDepartment=department;
        studentYear=year;
    }

    Student(string name){
        studentName=name;
    }

    void setDetails(string name, int rollNo, string department,int year){
        studentName=name;
        studentRollNo=rollNo;
        studentDepartment=department;
        studentYear=year;
    }

    void setDetailsFromTerminal(){
        string name; int rollNo; string department;int year;
        cout<<"ENTER\n";
        cout<<"Name       : ";
        getline(cin,name);
        cout<<"Roll No    : ";
        cin>>rollNo;
        cout<<"Department : ";
        cin>>department;
        cout<<"Year       : ";
        cin>>year;

        setStudentName(name);
        setstudentRollNo(rollNo);
        setstudentDepartment(department);
        setstudentYear(year);
//        studentName=name;
//        studentRollNo=rollNo;
//        studentDepartment=department;
//        studentYear=year;
    }

    void getDetails(){
        cout<<endl;
        cout<<"Details for student : "<<getStudentName()<<endl;
        cout<<"Roll No    : "<<getstudentRollNo()<<endl;
        cout<<"Department : "<<getstudentDepartment()<<endl;
        cout<<"Year       : "<<getstudentYear()<<endl;

        c1.setCourseName("Phuu");
        cout<<"-----coursename:"<<c1.getCourseName();
        cout<<endl;

    }
//------- setters
    void setStudentName(string name){
        studentName=name;
    }
    void setstudentDepartment(string department){
        studentDepartment=department;
    }
    void setstudentYear(int year){
        studentYear=year;
    }
    void setstudentRollNo(int rollNo){
        studentRollNo=rollNo;
    }

//------- getters
    string getStudentName(){
            return studentName;
    }
    string getstudentDepartment(){
            return studentDepartment;
    }
    int getstudentYear(){
            return studentYear;
    }
    int getstudentRollNo(){
            return studentRollNo;
    }


private:
    string studentName, studentDepartment;
    int studentRollNo, studentYear;
    Course c1;

};
