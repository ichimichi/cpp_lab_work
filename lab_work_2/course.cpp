class Course
{
public:
    Course(){
        courseName="x";
    }


    Course(string cname,string iname){
        courseName = cname;
        instructorName = iname;
    }

    void setCourseName(string name){
        courseName = name;
    }

    void setInstructorName(string name){
        instructorName = name;
    }
    string getCourseName(){
        return courseName;
    }
private:
    string courseName,instructorName;
};
