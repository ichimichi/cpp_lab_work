#include<iostream>
#include"Time.cpp" //including the source file of our Time class

using namespace std;

int main()
{   
    Time t1; //declaring object t1 of Time class
    int seconds; //to store seconds elapsed input for the user
    int ticks;  //to store the number of ticks from the user
    
    
    cout << "t1>" << endl;
    cout << "Enter the  seconds elapsed : " << endl;
    cin>>seconds;
    t1.setTime(seconds);    //calling the setTime() function with seconds as the seconds Elapsed
    t1.dispTimeStd();   //displaying the time in Standard format 
    cout << "Enter the  number ticks : " << endl;
    cin>>ticks;

    //calling tick() method , ticks number of times
    for(int i=1;i<=ticks;i++)
    {   
        t1.tick();
    }

    return 0;
}


