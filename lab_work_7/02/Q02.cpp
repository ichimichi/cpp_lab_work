#include <iostream>
#include "rectangle.cpp" //including the source file of our Rectangle class

int main()
{
    Rectangle r1; //declaring object r1 of Rectangle class
    xy p[3];      //array of struct xy to store the user entered coordinates

    // drawing a rectangle to give the user hint on how to enter the coordinates
    cout << "(x1,y1) ----------------- (x2,y2)" << endl;
    cout << "\t|\t\t|" << endl;
    cout << "\t|\t\t|" << endl;
    cout << "(x4,y4) ----------------- (x3,y3)" << endl;
    cout << endl;

    //taking input of the 4 coordinates from the user
    for (int i = 0; i < 4; i++)
    {
        cout << "Enter x" << i + 1 << " : ";
        cin >> p[i].x;
        cout << "Enter y" << i + 1 << " : ";
        cin >> p[i].y;
        cout << endl;
    }

    //checking if the points entered is a rectangle and whether the points lie in the first coordinate,
    //and not greater than 20.0
    //on returning true the specified properties of the the rectangle will be
    //displayed else the user will be prompt that the coordinates entered are not
    //that of a rectangle or the coordinates are out of bounds and the entered coordinates won't be set
    //to the object r1
    if (r1.set(p[0], p[1], p[2], p[3]))
    {
        cout << "Length:\t\t" << r1.getLength() << endl;
        cout << "Width :\t\t" << r1.getWidth() << endl;
        cout << "Perimeter:\t" << r1.getPerimeter() << endl;
        cout << "Area :\t\t" << r1.getArea() << endl;
        cout << "Square :\t" << (r1.checkIfSquare() ? "Yes" : "No") << endl;
    }

    return 0;
}