#include <iostream>
#include <cmath> //for abs() , sqrt()
using namespace std;

//user defined struct variable which
//stores the x and y coordinated of a point
struct xy
{
    double x;
    double y;
};

class Rectangle
{
  private:
    xy p1; //top left point
    xy p2; //top right point
    xy p3; //bottom right point
    xy p4; //bottom left point

  public:
    //parameterised constructor which calls the set() function
    Rectangle(xy i1, xy i2, xy i3, xy i4)
    {
        set(i1, i2, i3, i4);
    }

    //default constructor which initializes all the points to (0,0)
    Rectangle()
    {
        p1.x = 0.0;
        p1.y = 0.0;
        p2.x = 0.0;
        p2.y = 0.0;
        p3.x = 0.0;
        p3.y = 0.0;
        p4.x = 0.0;
        p4.y = 0.0;
    }

    //set() fuction which takes in four points i1,i2,i3,i4 and determines whether they belong to a
    //rectanglee and correspondingly assign to p1,p2,p3,p4 on being true
    bool set(xy i1, xy i2, xy i3, xy i4)
    {
        //checkIfRectangle() takes in four points and returns true if the points belong to a rectangle
        if (checkIfRectangle(i1, i2, i3, i4))
        {
            //if the points do belong to a rectangle we have to further check whether it is within bounds
            if (checkCoordinates(i1) && checkCoordinates(i2) && checkCoordinates(i3) && checkCoordinates(i4))
            {

                p1 = i1;
                p2 = i2;
                p3 = i3;
                p4 = i4;
                return true;
            }
        }
        else
        {
            cout << "Co-ordinates entered are not that of a rectangle" << endl;
            return false;
        }
    }

    //returns the perimeter of the rectangle
    double getPerimeter()
    {
        return 2 * (getLength() + getWidth());
    }

    //return the area of the rectangle
    double getArea()
    {
        return getLength() * getWidth();
    }

    //returns the width of the rectangle
    double getWidth()
    {
        double l1 = abs(sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2)));
        double l2 = abs(sqrt(pow(p3.x - p2.x, 2) + pow(p3.y - p2.y, 2)));
        return (l1 > l2) ? l2 : l1;
    }

    //returns the length of the rectangle
    double getLength()
    {
        double l1 = abs(sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2)));
        double l2 = abs(sqrt(pow(p3.x - p2.x, 2) + pow(p3.y - p2.y, 2)));
        return (l1 > l2) ? l1 : l2;
    }

    //check whether the rectangle is a square or not
    bool checkIfSquare()
    {
        double l1 = sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
        double l2 = sqrt(pow(p3.x - p2.x, 2) + pow(p3.y - p2.y, 2));
        double l3 = sqrt(pow(p4.x - p3.x, 2) + pow(p4.y - p3.y, 2));
        double l4 = sqrt(pow(p1.x - p4.x, 2) + pow(p1.y - p4.y, 2));

        if (l1 == l3 && l2 == l4 && l1 == l2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //checks if points i1,i2,i3,i4 are that of a rectangle
    bool checkIfRectangle(xy i1, xy i2, xy i3, xy i4)
    { //diagonal 1 , i.e, line i1-i3
        double d1 = abs(sqrt(pow(i3.x - i1.x, 2) + pow(i3.y - i1.y, 2)));
        //diagonal 1 , i.e, line i2-i4
        double d2 = abs(sqrt(pow(i2.x - i4.x, 2) + pow(i2.y - i4.y, 2)));

        //if the diagonal 1 and diagonal 2 are equal then we can say that it is possibly a rhombus or
        //a rectangle
        if (d1 == d2)
        { //checking whether the angle at the four corners are at 90 degress or not
            if (checkIfRight(i1, i2, i3) && checkIfRight(i2, i3, i4) && checkIfRight(i2, i4, i1) && checkIfRight(i4, i1, i2))
            {
                return true; //the four corners are all at right angles, hence it is a rectangle
            }
            else
            {
                return false; //the four corners are not at right angles, hence not a retangle but a rhombus
            }
        }
        else
        {
            return false; //since diagonal 1 and 2 are not equal, it cannot be a rectangle
        }
    }

    //checks whether the point i1 is in between 0 and 20.0
    bool
    checkCoordinates(xy i1)
    {
        if ((i1.x >= 0.0 && i1.x <= 20.0) || (i1.y >= 0.0 && i1.y <= 20.0))
        {
            return true;
        }
        else
        {
            cout << "Co-ordinates out of bounds" << i1.x << "," << i1.y << endl;
            return false;
        }
    }

    //checks the angle between line i1-i2 and line i3-i2
    //at point i2 is 90 degress or not
    bool checkIfRight(xy i1, xy i2, xy i3)
    {
        //slope of line i1-i2
        double m1 = (i2.y - i1.y) / (i2.x - i1.x);
        //slope of line i3-i2
        double m2 = (i3.y - i2.y) / (i3.x - i2.x);
        //theta is the angle between line i1-i2 and line i3-i2
        double theta = atan(abs(m1 - m2) / (1 + m1 * m2));

        //since tan(90 deg) is undefined then
        if (theta == theta)
        {
            //theta is a number therefore we can conclude that the angle between the two lines is
            //not 90 degrees
            return false;
        }
        else
        {
            //if theta != theta then we can say that theta is NaN(Not a number)
            //i.e, theta is 90 degrees
            return true;
        }
    }
};