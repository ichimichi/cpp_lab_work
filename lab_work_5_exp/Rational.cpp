#include <iostream>
#include "rational.h"
using namespace std;

Rational::Rational()
{
    numerator = 0;
    denominator = 0;
}

Rational::Rational(int n, int d)
{
    if (d == 0)
    {
        cout << "Object Creation failed , denominator can't be zero\n"
             << endl;
    }

    numerator = n;
    denominator = (numerator == 0) ? 1 : d;

    reduce();
}

void Rational::addRational(Rational x1, Rational x2)
{
    if (x1.numerator == 0 || x2.numerator == 0)
    {
        if (x1.numerator != 0)
        {
            numerator = x1.numerator;
            denominator = x1.denominator;
        }
        else
        {
            numerator = x2.numerator;
            denominator = x2.denominator;
        }
    }
    else
    {
        if (x1.denominator != x2.denominator)
        {

            denominator = lcm(x1.denominator, x2.denominator);
            int c = (denominator / x1.numerator);
            int d = (denominator / x2.numerator);

            if (x2.denominator == 1)
            {
                d = x1.denominator;
                c = 1;
            }
            if (x1.denominator == 1)
            {
                c = x2.denominator;
                d = 1;
            }
            numerator = x1.numerator * c + x2.numerator * d;
        }
        else
        {
            denominator = x1.denominator;
            numerator = x1.numerator + x2.numerator;
        }
    }
    reduce();
}

void Rational::subRational(Rational x1, Rational x2)
{
    if (x1.numerator == 0 || x2.numerator == 0)
    {
        if (x1.numerator != 0)
        {
            numerator = x1.numerator;
            denominator = x1.denominator;
        }
        else
        {
            numerator = -x2.numerator;
            denominator = x2.denominator;
        }
    }
    else
    {
        if (x1.denominator != x2.denominator)
        {

            denominator = lcm(x1.denominator, x2.denominator);
            int c = (denominator / x1.numerator);
            int d = (denominator / x2.numerator);

            if (x2.denominator == 1)
            {
                d = x1.denominator;
                c = 1;
            }
            if (x1.denominator == 1)
            {
                c = x2.denominator;
                d = 1;
            }

            numerator = x1.numerator * c - x2.numerator * d;
        }
        else
        {
            denominator = x1.denominator;
            numerator = x1.numerator - x2.numerator;
        }
    }
    reduce();
}

void Rational::mulRational(Rational x1, Rational x2)
{
    numerator = x1.numerator * x2.numerator;
    denominator = x1.denominator * x2.denominator;
    reduce();
}

void Rational::divRational(Rational x1, Rational x2)
{

    numerator = x1.numerator * x2.denominator;
    denominator = x1.denominator * x2.numerator;
    reduce();
}

void Rational::reduce()
{
    int k;
    if ((k = gcd(numerator, denominator)) != 1 && numerator != 0)
    {
        if (k > 0)
        {
            numerator = numerator / k;
            denominator = denominator / k;
        }
        else
        {
            numerator = -numerator / k;
            denominator = -denominator / k;
        }
    }
}

int Rational::lcm(int n1, int n2)
{
    int max = (n1 > n2) ? n1 : n2;
    do
    {
        if (max % n1 == 0 && max % n2 == 0)
        {
            return max;
            break;
        }
        else
            ++max;
    } while (true);
}

int Rational::gcd(int a, int b)
{
    if (b == 0)
        return a;

    gcd(b, a % b);
}

void Rational::displayabform()
{
    if (denominator == 0)
    {
        cout << "NaN";
    }
    else if (numerator != 0)
        cout << numerator << "/" << denominator;
    else
        cout << 0;
}

void Rational::displaypointform()
{
    if (denominator == 0)
    {
        cout << "NaN";
    }
    else if (numerator != 0)
        cout << (float)numerator / (float)denominator;
    else
        cout << 0;
}
