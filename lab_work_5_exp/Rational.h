#ifndef RATIONAL_H
#define RATIONAL_H

class Rational
{
  private:
    int numerator;
    int denominator;

  public:
    Rational();

    Rational(int, int);

    void addRational(Rational, Rational);

    void subRational(Rational, Rational);

    void mulRational(Rational, Rational);
    void divRational(Rational, Rational);

    void reduce();

    int lcm(int, int);

    int gcd(int, int);

    void displayabform();

    void displaypointform();
};

#endif //RATIONAL_H