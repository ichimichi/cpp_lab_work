#include<iostream>

using namespace std;


void func1(int * ptr);//non constant pointer to non constant data
void func2(const int *  ptr);//non constant pointer to constant data
void func3(int * const ptr);//constant pointer to non constant data
void func4(const int * const ptr);//constant pointer to constant data

int main(){
    int x = 5;

    func1(&x);
    func2(&x);
    func3(&x);
    func4(&x);

    return 0;
}

void func1(int * ptr){
    int y = *ptr;
    *ptr=10;
    ptr=&y;
    cout << "--non-constant pointer to a non-constant data" << endl;
    cout << "ptr: "<< ptr << endl;
    cout << "*ptr: "<< *ptr << endl;
    cout << "y: "<< y << endl;
}

void func2(const int * ptr){
    int y = *ptr;
    *ptr=10;
    ptr=&y;
    cout << "\nnon-constant pointer to a constant data" << endl;
    cout << "ptr: "<< ptr << endl;
    cout << "*ptr: "<< *ptr << endl;
    cout << "y: "<< y << endl;
}
void func3(int * const ptr){
    int y = *ptr;
    *ptr=10;
    ptr=&y;
    cout << "\nconstant pointer to a non-constant data" << endl;
    cout << "ptr: "<< ptr << endl;
    cout << "*ptr: "<< *ptr << endl;
    cout << "y: "<< y << endl;
}
void func4(const int * const ptr){
    int y = *ptr;
    *ptr=10;
    ptr=&y;
    cout << "constant pointer to a constant data" << endl;
    cout << "ptr: "<< ptr << endl;
    cout << "*ptr: "<< *ptr << endl;
    cout << "y: "<< y << endl;
}