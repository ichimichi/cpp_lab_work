// MAX indicates the maximum number of terms capable of being store
// if MAX = 200, then Polynomial can have upto 100 terms
#define MAX 200

class Polynomial
{
  private:
    int terms[MAX]; // terms[] array stores coefficient and exponent in pairs
                    // one after another
    int noOfTerms;  // the number of terms in the polynomial

  public:
    // our default and parameterised constructor
    Polynomial(int x[] = {}, int n = 0)
    {
        setPoly(x, n);
    }

    // sets the terms[] array to that of the data array x[]
    // followed by sorting the terms according to increasing exponents
    // and then simplifying the polynomial
    // and finally shifting the terms to the left if any coefficient
    // is found to be zero '0'
    void setPoly(int x[], int n)
    {
        for (int i = 0; i < MAX; i++)
        {
            terms[i] = 0;
        }

        for (int i = 0; i < n * 2; i++)
        {
            terms[i] = x[i];
        }
        noOfTerms = n;
        sortPoly(terms, n);
        simpPoly();
        remZeroTerms();
    }

    // overloading the = operator
    Polynomial operator=(Polynomial const &other)
    {
        for (int i = 0; i < other.noOfTerms * 2; i++)
        {
            terms[i] = other.terms[i];
        }
        noOfTerms = other.noOfTerms;
    }

    // overloading the + operator to add two Polynomials
    Polynomial operator+(Polynomial const &other)
    {
        Polynomial temp;
        int x[200], i;
        for (i = 0; i < noOfTerms * 2; i++)
        {
            x[i] = terms[i];
        }
        for (int j = noOfTerms * 2, i = 0; j < (noOfTerms + other.noOfTerms) * 2; j++, i++)
        {
            x[j] = other.terms[i];
        }

        temp.setPoly(x, noOfTerms + other.noOfTerms);
        return temp;
    }

    // overloading the + operator to subtract two Polynomials
    Polynomial operator-(Polynomial const &other)
    {
        Polynomial temp;
        int x[200], i;
        for (i = 0; i < noOfTerms * 2; i++)
        {
            x[i] = terms[i];
        }
        for (int j = noOfTerms * 2, i = 0; j < (noOfTerms + other.noOfTerms) * 2; j++, i++)
        {
            x[j] = (i % 2 == 0) ? -other.terms[i] : other.terms[i];
        }

        temp.setPoly(x, noOfTerms + other.noOfTerms);
        return temp;
    }

    // overloading the + operator to multiply two Polynomials
    Polynomial operator*(Polynomial const &other)
    {
        Polynomial temp;
        int x[200], i = 0, j, k;

        for (j = 0; j < noOfTerms * 2; j += 2)
        {
            for (k = 0; k < other.noOfTerms * 2; k++)
            {
                x[i] = (i % 2 == 0) ? terms[j] * other.terms[k] : terms[j + 1] + other.terms[k];
                i++;
            }
        }

        temp.setPoly(x, noOfTerms * other.noOfTerms);
        return temp;
    }

    // overloading the += operator to add two Polynomials and assign
    // the result to the first Polynomial operand
    Polynomial operator+=(Polynomial const &other)
    {
        Polynomial temp;
        temp = *this + other;
        *this = temp;
    }

    // overloading the -= operator to subtract two Polynomials and assign
    // the result to the first Polynomial operand
    Polynomial operator-=(Polynomial const &other)
    {
        Polynomial temp;
        temp = *this - other;
        *this = temp;
    }

    // overloading the += operator to multiply two Polynomials and assign
    // the result to the first Polynomial operand
    Polynomial operator*=(Polynomial const &other)
    {
        Polynomial temp;
        temp = *this * other;
        *this = temp;
    }

    void dispPoly()
    {
        int i;

        if (noOfTerms == 0)
        {
            std::cout << terms[0] << std::endl;
            return;
        }
        for (i = noOfTerms * 2 - 1; i > 1; i -= 2)
        {
            std::cout << terms[i - 1] << "x^" << terms[i] << " + ";
        }
        if (terms[i] != 0)
        {
            std::cout << terms[i - 1] << "x^" << terms[i] << std::endl;
        }
        else
        {
            std::cout << terms[i - 1] << std::endl;
        }
    }

  private:
    /*
    //
    // Helper member functions
    //
    */

    bool expoentMatch(int x, Polynomial &temp)
    {
        for (int i = 0; i < temp.noOfTerms * 2; i += 2)
        {
            if (x == temp.terms[i])
            {
                return true;
            }
        }
        return false;
    }

    void sortPoly(int x[], int n)
    {
        for (int i = 0; i < n * 2; i += 2)
        {
            for (int j = 0; j < n * 2 - i - 2; j += 2)
            {
                if (x[j + 1] > x[j + 3])
                {
                    int tempE = x[j + 3];
                    int tempC = x[j + 2];
                    x[j + 3] = x[j + 1];
                    x[j + 2] = x[j];
                    x[j + 1] = tempE;
                    x[j] = tempC;
                }
            }
        }
    }

    void simpPoly()
    {
        for (int i = 0; i < noOfTerms * 2; i += 2)
        {
            for (int j = 0; j < noOfTerms * 2; j += 2)
            {
                if (terms[i + 1] == terms[j + 1] && i + 1 != j + 1)
                {
                    terms[i] += terms[j];

                    for (int k = j; k < noOfTerms * 2; k += 2)
                    {
                        terms[k] = terms[k + 2];
                        terms[k + 1] = terms[k + 3];
                    }
                    noOfTerms--;
                }
            }
        }
    }

    void remZeroTerms()
    {
        for (int i = 0; i < noOfTerms * 2; i += 2)
        {
            if (terms[i] == 0)
            {

                for (int k = i; k < noOfTerms * 2; k += 2)
                {
                    terms[k] = terms[k + 2];
                    terms[k + 1] = terms[k + 3];
                }
                noOfTerms--;
            }
        }
    }
};
