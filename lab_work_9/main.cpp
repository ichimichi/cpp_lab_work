// driver Program
#include <iostream>
using namespace std;
#include "Polynomial.cpp"

// function helps to take input from the user to create objects
// of class Polynomial
void getInput(int data[],int &noOfTerms){
    noOfTerms = -1;
    do{ 
        noOfTerms++;
        cout<<"Coefficient : ";
        cin>>data[noOfTerms*2];
        if(data[noOfTerms*2]==0){
            break;
        }
        cout<<"Exponent : ";
        cin>>data[noOfTerms*2+1];
    }while(true);
}

int main()
{
    int data[100];
    int noOfTerms;

    cout<<"---  E n t e r  C o e f f i c i e n t   a s  '0'  t o   s t o p  ---"<<endl<<endl;

    getInput(data,noOfTerms);
    Polynomial p1(data,noOfTerms);
    cout<<endl<<"p1 : ";
    p1.dispPoly();

    cout<<endl;

    getInput(data,noOfTerms);
    Polynomial p2(data,noOfTerms);
    cout<<endl<<"p2 : ";
    p2.dispPoly();

    cout<<endl;

    Polynomial p3;    
    p3 = p1;
    cout<<"p3 = p1 : ";
    p3.dispPoly();

    cout<<endl;

    p3 = p1+p2;
    cout<<"p3 = p1 + p2 : ";
    p3.dispPoly();

    p3 = p1-p2;
    cout<<"p3 = p1 - p2 : ";
    p3.dispPoly();

    p3 = p1*p2;
    cout<<"p3 = p1 * p2 : ";
    p3.dispPoly();

    cout<<endl;

    p1 += p2;
    cout<<"p1 += p2 : ";
    p1.dispPoly();

    p1 -= p2;
    cout<<"p1 -= p2 : ";
    p1.dispPoly();

    p1 *= p2;
    cout<<"p1 *= p2 : ";
    p1.dispPoly();

    return 0;
}