class Movie
{
public:
    Movie(){
        movieTitle = "not defined";
        movieRating = 0.0;
    }

    Movie(string title, float rating){
        movieTitle = title;
        movieRating = rating;
    }

    string getMovieTitle() {
        return movieTitle;
    }
    float getMovieRating() {
        return movieRating;
        }
private:
    string movieTitle = "hello";
    float movieRating;
};
