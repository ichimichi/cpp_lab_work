//source code of our HugeInteger class
#include <iostream>
#include <string>
using namespace std;
#include "HugeInteger.h"

HugeInteger::HugeInteger(string x)
{
    in(x);
}

void HugeInteger::in(string x)
{
    unsigned i, j;
    for (i = x.length(), j = 0; i > 0; i--, j++)
    {
        position[j] = x.at(i - 1) - '0';
    }
    noOfDigits = x.length();
}

void HugeInteger::out()
{
    for (int i = noOfDigits - 1; i >= 0; i--)
    {
        cout << position[i];
    }
    cout << endl;
}

void HugeInteger::add(long long int number)
{
    int i = 0;
    position[i] += number;
    for (i = 0; i < 40; i++)
    {
        if (position[i] >= 10)
        {
            position[i + 1] += position[i] / 10;
            position[i] = position[i] % 10;
        }
    }
    updateNoOfDigits();
}

void HugeInteger::sub(long long int number)
{
    int i = 0;

    for (i = 0; i < noOfDigits; i++)
    {
        position[i] -= (number % 10);
        if (position[i] < 0)
        {
            position[i] += (number % 10);
            position[i] += 10;
            position[i] -= (number % 10);
            position[i + 1]--;
        }
        number /= 10;
    }

    if (position[noOfDigits] < 0)
    {
        position[0] = position[0] - 9 + position[noOfDigits];
        for (i = noOfDigits; i > 0; i--)
        {
            position[i] = 0;
        }
    }

    updateNoOfDigits();
}

bool HugeInteger::isEqual(const HugeInteger other)
{
    if (noOfDigits == other.noOfDigits)
    {
        for (int i = noOfDigits; i >= 0; i--)
        {
            if (position[i] != other.position[i])
            {
                return false;
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool HugeInteger::isNotEqualTo(const HugeInteger other)
{
    if (noOfDigits == other.noOfDigits)
    {
        for (int i = noOfDigits; i >= 0; i--)
        {
            if (position[i] != other.position[i])
            {
                return true;
            }
        }
        return false;
    }
    else
    {
        return true;
    }
}

bool HugeInteger::isGreaterThan(const HugeInteger other)
{
    if (noOfDigits > other.noOfDigits)
    {
        return true;
    }
    else if (noOfDigits < other.noOfDigits)
    {
        return false;
    }
    else
    {
        for (int i = noOfDigits; i >= 0; i--)
        {
            if (position[i] > other.position[i])
            {
                return true;
            }
        }
        return false;
    }
}

bool HugeInteger::isGreaterThanOrEqualTo(const HugeInteger other)
{
    if (noOfDigits > other.noOfDigits)
    {
        return true;
    }
    else if (noOfDigits < other.noOfDigits)
    {
        return false;
    }
    else
    {
        for (int i = noOfDigits; i >= 0; i--)
        {
            if (position[i] >= other.position[i])
            {
                return true;
            }
        }
        return false;
    }
}

bool HugeInteger::isLessThan(const HugeInteger other)
{
    if (noOfDigits < other.noOfDigits)
    {
        return true;
    }
    else if (noOfDigits > other.noOfDigits)
    {
        return false;
    }
    else
    {
        for (int i = noOfDigits; i >= 0; i--)
        {
            if (position[i] < other.position[i])
            {
                return true;
            }
        }
        return false;
    }
}

bool HugeInteger::isLessThanOrEqualTo(const HugeInteger other)
{
    if (noOfDigits < other.noOfDigits)
    {
        return true;
    }
    else if (noOfDigits > other.noOfDigits)
    {
        return false;
    }
    else
    {
        for (int i = noOfDigits; i >= 0; i--)
        {
            if (position[i] <= other.position[i])
            {
                return true;
            }
        }
        return false;
    }
}

void HugeInteger::updateNoOfDigits()
{
    for (int i = 40 - 1; i >= 0; i--)
    {
        if (position[i] != 0)
        {
            noOfDigits = i + 1;
            break;
        }
    }
}
