//header file of our HugeInteger class
#ifndef HUGEINTEGER_H
#define HUGEINTEGER_H

class HugeInteger
{
  public:
    //can store upto 40-digit number where each digit is between 0-9
    int position[40] = {0};
    //stores the number of digits of a HugeInteger Object
    int noOfDigits;

    //parameterised constructor which takes in a string of numbers upto 40-digits
    //and calls the in() member function which
    //parses the string and stores each digit in the corresponding decimal position
    //in position[] array
    HugeInteger(string);

    //parses the string and stores each digit in the corresponding decimal position
    //in position[] array
    void in(string);

    //displays the HugeInteger number which can be of upto 40-digits long
    void out();

    //adds a number of type long long int to the HugeInteger Number
    void add(long long int);

    //subtracts a number of type long long int from the HugeInteger Number
    void sub(long long int);

    //returns true if the HugeInteger object passed as an argument is equal to
    //the object which invokes it  
    bool isEqual(const HugeInteger);

    //returns true if the HugeInteger object passed as an argument is not equal to
    //the object which invokes it  
    bool isNotEqualTo(const HugeInteger);

    //returns true if the HugeInteger object passed as an argument is greater than
    //the object which invokes it  
    bool isGreaterThan(const HugeInteger);

    //returns true if the HugeInteger object passed as an argument is greater than or equal to
    //the object which invokes it  
    bool isGreaterThanOrEqualTo(const HugeInteger);

    //returns true if the HugeInteger object passed as an argument is less than
    //the object which invokes it  
    bool isLessThan(const HugeInteger);

    //returns true if the HugeInteger object passed as an argument is less than or equal to
    //the object which invokes it  
    bool isLessThanOrEqualTo(const HugeInteger);

    //this function helps in updating the noOfDigits after an arithmentic operation 
    void updateNoOfDigits();
};

#endif //HUGEINTEGER_H