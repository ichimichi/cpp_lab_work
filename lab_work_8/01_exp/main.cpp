#include <iostream>
#include <string>
using namespace std;
#include "HugeInteger.h"


int main(){
    HugeInteger h("12345678910111213141516171819202122232");
    HugeInteger f("12345678910111213141516171819202122232");
    HugeInteger g("238988876455787569723437874587435834758");
    h.out();
    f.out();
    g.out();
    cout<<endl;

    cout<<h.isEqual(f)<<endl;
    cout<<h.isNotEqualTo(f)<<endl;
    cout<<h.isGreaterThan(f)<<endl;
    cout<<h.isGreaterThanOrEqualTo(f)<<endl;
    cout<<h.isLessThanOrEqualTo(f)<<endl;
    cout<<h.isLessThan(f)<<endl;
    cout<<endl;
    h.add(988776677889);
    h.out();
    h.sub(6171820136322041);
    h.out();


    return 0;
}