#include <iostream>
#include <cmath>
using namespace std;

class HugeInteger
{
  public:
    int position[40] = {0};
    int noOfDigits;

  public:
    HugeInteger(string x)
    {
        in(x);
    }

    void in(string x)
    {
        unsigned i, j;
        for (i = x.length(), j = 0; i > 0; i--, j++)
        {
            position[j] = x.at(i - 1) - '0';
        }
        noOfDigits = x.length();
    }

    void out()
    {
        for (int i = noOfDigits - 1; i >= 0; i--)
        {
            cout << position[i];
        }
        cout << endl;
    }

    void add(long long int number)
    {
        int i = 0;
        position[i] += number;
        for (i = 0; i < 40; i++)
        {
            if (position[i] >= 10)
            {
                position[i + 1] += position[i] / 10;
                position[i] = position[i] % 10;
            }
        }
        updateNoOfDigits();
    }

    void sub(long long int number)
    {
        int i = 0;

        for (i = 0; i < noOfDigits; i++)
        {
            position[i] -= (number % 10);
            if (position[i] < 0)
            {
                position[i] += (number % 10);
                position[i] += 10;
                position[i] -= (number % 10);
                position[i + 1]--;
            }
            number /= 10;
        }

        if (position[noOfDigits] < 0)
        {
            position[0] = position[0] - 9 + position[noOfDigits];
            for (i = noOfDigits; i > 0; i--)
            {
                position[i] = 0;
            }
        }

        updateNoOfDigits();
    }

    bool isEqual(const HugeInteger other)
    {
        if (noOfDigits == other.noOfDigits)
        {
            for (int i = noOfDigits; i >= 0; i--)
            {
                if (position[i] != other.position[i])
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    bool isNotEqualTo(const HugeInteger other)
    {
        if (noOfDigits == other.noOfDigits)
        {
            for (int i = noOfDigits; i >= 0; i--)
            {
                if (position[i] != other.position[i])
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            return true;
        }
    }

    bool isGreaterThan(const HugeInteger other)
    {
        if (noOfDigits > other.noOfDigits)
        {
            return true;
        }
        else if (noOfDigits < other.noOfDigits)
        {
            return false;
        }
        else
        {
            for (int i = noOfDigits; i >= 0; i--)
            {
                if (position[i] > other.position[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    bool isGreaterThanOrEqualTo(const HugeInteger other)
    {
        if (noOfDigits > other.noOfDigits)
        {
            return true;
        }
        else if (noOfDigits < other.noOfDigits)
        {
            return false;
        }
        else
        {
            for (int i = noOfDigits; i >= 0; i--)
            {
                if (position[i] >= other.position[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    bool isLessThan(const HugeInteger other)
    {
        if (noOfDigits < other.noOfDigits)
        {
            return true;
        }
        else if (noOfDigits > other.noOfDigits)
        {
            return false;
        }
        else
        {
            for (int i = noOfDigits; i >= 0; i--)
            {
                if (position[i] < other.position[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    bool isLessThanOrEqualTo(const HugeInteger other)
    {
        if (noOfDigits < other.noOfDigits)
        {
            return true;
        }
        else if (noOfDigits > other.noOfDigits)
        {
            return false;
        }
        else
        {
            for (int i = noOfDigits; i >= 0; i--)
            {
                if (position[i] <= other.position[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    void updateNoOfDigits()
    {
        for (int i = 40 - 1; i >= 0; i--)
        {
            if (position[i] != 0)
            {
                noOfDigits = i + 1;
                break;
            }
        }
    }
};